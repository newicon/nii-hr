<?php

class HrAdminController extends AController {

	public $name = 'holiday';
	public $modelName = 'HrHoliday';

	/**
	 * Lists all models.
	 */
	public function actionIndex() {
		$model = new $this->modelName('search');
		$model->unsetAttributes();

		if (isset($_GET[$this->modelName]))
			$model->attributes = $_GET[$this->modelName];

		$this->render('index', array(
			'dataProvider' => $model->search(),
			'model' => $model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate() {
		$model = new $this->modelName;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model, $this->modelName.'Form');

		if (isset($_POST[$this->modelName])) {
			$model->attributes = $_POST[$this->modelName];
			if ($model->save())
				$this->redirect(array('view', 'id' => $model->id));
		}

		$this->render('create', array('model' => $model));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionEdit($id) {
		$model = NData::loadModel($this->modelName, $id, '<strong>No ' . $this->name . ' exists for ID: ' . $id . '</strong>');

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model, $this->modelName.'Form');

		if (isset($_POST[$this->modelName])) {
			$model->attributes = $_POST[$this->modelName];
			if ($model->save())
				$this->redirect(array('view', 'id' => $model->id));
		}

		$this->render('edit', array('model' => $model));
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id) {
		$model = NData::loadModel($this->modelName, $id, '<strong>No ' . $this->name . ' exists for ID: ' . $id . '</strong>');

		$this->render('view', array('model' => $model));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id) {
		if (Yii::app()->request->isPostRequest) {
			
			// we only allow deletion via POST request
			$model = NData::loadModel($this->modelName, $id, '<strong>No ' . $this->name . ' exists for ID: ' . $id . '</strong>');
			$model->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if (!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
		}
		else
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
	}

}
