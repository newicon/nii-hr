<?php

class HrHelper {

	static public function staffViewLink($staff, $label=null) {
		if($staff){
			if(!$label)
				$label = $staff->name;
			return CHtml::link($label, array('/hr/staff/view', 'id' => $staff->id));
		}
		return $label;
	}

	static public function staffEditLink($staff, $label=null) {
		if($staff){
			if(!$label)
				$label = $staff->name;
			return CHtml::link($label, array('/hr/staff/edit', 'id' => $staff->id));
		}
		return $label;
	}

	static public function holidayViewLink($model) {
		return CHtml::link(NHtml::formatDate($model->date), array('/hr/holiday/view', 'id' => $model->id));
	}

	static public function holidayEditLink($model) {
		return CHtml::link(NHtml::formatDate($model->date), array('/hr/holiday/edit', 'id' => $model->id));
	}

	static public function holidayRequestManagerLink($model, $label=null) {
		$dates = $model->requestDates;
		if (empty($dates))
			return '<span class="noData">--- Malformed request --- </span>';
		sort($dates);
		$count = count($dates)-1;
		$firstDate = NHtml::formatDate($dates[0]['date']);
		$lastDate = NHtml::formatDate($dates[$count]['date']);
		$label = $label!==null ? $label : $firstDate . ($lastDate==$firstDate?'':' - ' . $lastDate);
		return CHtml::link($label, NHtml::urlAbsolute(array('/hr/holiday/request', 'id' => $model->id)));
	}
	
	static public function holidayRequestManagerApproveLink($model, $label=null) {
		$label = $label!==null ? $label : 'Approve Request #'.$model->id;
		return CHtml::link($label, NHtml::urlAbsolute(array('/hr/holiday/approve', 'id' => $model->id)));
	}

	static public function holidayRequestViewLink($model, $label=null) {
		$dates = $model->requestDates;
		if (empty($dates))
			return '<span class="noData">--- Malformed request --- </span>';
		sort($dates);
		$count = count($dates)-1;
		$firstDate = NHtml::formatDate($dates[0]['date']);
		$lastDate = NHtml::formatDate($dates[$count]['date']);
		$label = $label!==null ? $label : $firstDate . ($lastDate==$firstDate?'':' - ' . $lastDate);
		return CHtml::link($label, NHtml::urlAbsolute(array('/hr/myholiday/request', 'id' => $model->id)));
	}

	static public function staffDropDownData($exclude_id=null) {
		$criteria = array('with' => 'user','together' => true,'order' => 'user.last_name');
		if($exclude_id > 0)
			$criteria['condition'] = 't.id != '.$exclude_id;
		return CHtml::listData(HrStaff::model()->findAll($criteria), 'id', 'name');
	}
	
	static public function managerDropDownData($exclude_id=null){
		return self::staffDropDownData($exclude_id);
	}

	static public function userDropDownData() {
		return CHtml::listData(User::model()->findAll(array('order' => 'last_name')), 'id', 'name');
	}

	static public function durationDropDownData() {
		return array(
			'full' => 'Full Day',
			'am' => 'Half Day - AM',
			'pm' => 'Half Day - PM',
		);
	}

	static public function requestStateDropdown() {
		return array(
			HrHolidayRequest::REQUESTED => 'Requested',
			HrHolidayRequest::APPROVED => 'Approved',
			HrHolidayRequest::REJECTED => 'Rejected',
		);
	}

	static public function holidayBookedDates() {
		return CHtml::listData(HrHoliday::model()->findAllByAttributes(array('staff_id'=>self::userStaffRecord()->id)), 'date', 'date');
	}

	static public function bankHolidayDates($year=null) {
		$condition = '';
		if ($year!==null)
			$condition = 'date LIKE "'.$year.'-%"';
		return CHtml::listData(HrBankHoliday::model()->findAll($condition), 'date', 'date');
	}


	static public function checkDate($date) {
	    $disabledDates = array_merge(self::bankHolidayDates(), self::holidayBookedDates());
	    while (in_array($date, $disabledDates) || date('N',strtotime($date)) >= 6) {
			$udate = strtotime($date);
			$date = date('Y-m-d', mktime(0, 0, 0, date("m",$udate)  , date("d",$udate)+1, date("Y",$udate)));
	    }
	    return $date;
	}

	static public function holidayDuration($duration, $period=null) {
		$duration = (string) (float) $duration; //To fix decimal place issues
		$durations = self::durationDropDownData();
		return isset($durations[$duration]) ? $durations[$duration] : $duration . ' day' . ($duration=='1'?'':'s') . ($duration=='0.5'?' ('.$period.')' : '');
	}
	
	static public function getUserHolidaysYear($year, $staff_id, $includeAllStaffInfo=false) {
		$holidays=array('dates'=>array(), 'staff'=>array());
		$bankHolidayDates = HrBankHoliday::model()->findAll('date LIKE "'.$year.'-%"');
		foreach ($bankHolidayDates as $bhd)
			$holidays['dates'][$bhd->date] = array('count'=>0, 'type'=>'bh', 'comment'=>$bhd->description);
		$bookedHolidays = HrHoliday::model()->findAll(array('condition'=>'date LIKE "'.$year.'-%" AND staff_id ='.$staff_id, 'select'=>array('date', '(SELECT count(id) FROM hr_holiday WHERE date = t.date) as bookedCount', 'state', 'request_id'), 'group'=>'date'));
		$staff=array();
		if ($includeAllStaffInfo==true) {
			$staffHolidays = HrHoliday::model()->with('staff')->findAll(array('condition'=>'date LIKE "'.$year.'-%"'));
			foreach ($staffHolidays as $sh)
				$staff[$sh->date][] = $sh->staff->name;
		}
		foreach ($bookedHolidays as $holiday)
			$holidays['dates'][$holiday->date] = array('count'=>$holiday->bookedCount, 'type'=>$holiday->stateLabel, 'comment'=>'', 'request_id'=>$holiday->request_id, 'state'=>$holiday->state);
		$holidays['staff'] = $staff;
		return $holidays;
	}

	static public function getAllUserHolidaysYear($year, $includeStaffInfo=false) {
		$holidays=array('dates'=>array(), 'staff'=>array());
		$bankHolidayDates = HrBankHoliday::model()->findAll('date LIKE "'.$year.'-%"');
		foreach ($bankHolidayDates as $bhd)
			$holidays['dates'][$bhd->date] = array('count'=>0, 'type'=>'bh', 'comment'=>$bhd->description);
		$bookedHolidays = HrHoliday::model()->findAll(array('condition'=>'date LIKE "'.$year.'-%"', 'select'=>array('date', 'count(id) as bookedCount'), 'group'=>'date'));
		$staff=array();
		if ($includeStaffInfo==true) {
			$staffHolidays = HrHoliday::model()->with('staff')->findAll(array('condition'=>'date LIKE "'.$year.'-%"'));
			foreach ($staffHolidays as $sh)
				$staff[$sh->date][] = $sh->staff->name;
		}
		foreach ($bookedHolidays as $holiday)
			$holidays['dates'][$holiday->date] = array('count'=>$holiday->bookedCount, 'type'=>'', 'comment'=>'');
		$holidays['staff'] = $staff;
		return $holidays;
	}

	static public function countBookedForDate($date, $exclude_id=0) {
		$count = HrHoliday::model()->count('date = "'.$date.'" AND staff_id <> '.$exclude_id);
		return $count;
	}

	static public function countLabelClass($count) {
		switch($count) {
			case 0 : 
				$class='success';
				break;
			case 1 :
			case 2 :
				$class='primary';
				break;
			case 3 :
				$class='warning';
				break;
			default :
				$class='important';
		}
		return $class;
	}

	static public function requestStateLabelClass($state) {
		switch($state) {
			case HrHolidayRequest::APPROVED : 
				$class = 'success';
				break;
			case HrHolidayRequest::REJECTED : 
				$class = 'important';
				break;
			default :
				$class = 'primary';
		}
		return $class;
	}

	static public function isWeekend($date) {
		return (date('N', strtotime($date)) >= 6);
	}

	static public function userStaffRecord() {
		return HrStaff::model()->find(array('condition'=>'user_id='.Yii::app()->user->record->id));
	}
	
	static public function isManager($staff,$manager=null){
		if($staff){
			$manager = $manager ? $manager : self::userStaffRecord();
			return $staff->manager_id == $manager->id;
		}
		return false;
	}
	
	static public function getStaffHolidayAllowance($year=null, $staff=null) {
		$year = $year ? $year : date('Y');
		if ($staff==null)
			$staff = self::userStaffRecord();
		$allowance = HrStaffHolidayAllowance::model()->findByAttributes(array('staff_id'=>$staff->id, 'year'=>$year));
		if ($allowance) {
			return $allowance->allowance;
		} else {
			$allowance = HrStaffHolidayAllowance::model()->findByAttributes(array('staff_id'=>0, 'year'=>$year));
			if ($allowance) {
				return $allowance->allowance;
			} else {
				return $staff->holiday_allowance;
			}
		}
	}

}