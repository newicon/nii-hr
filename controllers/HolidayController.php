<?php

class HolidayController extends HrAdminController {

	public $modelName = 'HrHolidayRequest';
	public $name = 'holiday request';
	
	public function actionCreate() {
		
		$model = new $this->modelName;
		$model->staff_id = HrHelper::userStaffRecord()->id;

		if (isset($_POST[$this->modelName])) {

			$request = new HrHolidayRequest;
			$request->staff_id = $_POST[$this->modelName]['staff_id'];
			$request->state = 0;
			$request->requested_at = date('Y-m-d H:i:s');
			if ($request->save()) {
				foreach ($_POST['HrHoliday']['date'] as $count => $date) {
					$model = new HrHoliday;
					$model->staff_id = $_POST[$this->modelName]['staff_id'];
					$model->date = $date;
					$model->duration = $_POST['HrHoliday']['duration'][$count];
					$model->half_day_period = $_POST['HrHoliday']['half_day_period'][$count];
					$model->request_id = $request->id;
					$model->save();
				}
			}
			$this->redirect(array('index'));
		}

		$this->render('create', array('model' => $model));
	}
	
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionEdit($id) {
		$model = NData::loadModel($this->modelName, $id, '<strong>No ' . $this->name . ' exists for ID: ' . $id . '</strong>');

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model, $this->modelName.'Form');

		if (isset($_POST[$this->modelName])) {
			$model->attributes = $_POST[$this->modelName];
			if ($model->save())
				$this->redirect(array('index'));
		}

		$this->render('edit', array('model' => $model));
	}
	
	public function actionApprove($id) {
		$model = NData::loadModel($this->modelName, $id, '<strong>No ' . $this->name . ' exists for ID: ' . $id . '</strong>');
		if ($model)
			$model->approve();
		$this->redirect('/hr/holiday/index');
	}
	
	public function actionReject($id) {
		$model = NData::loadModel($this->modelName, $id, '<strong>No ' . $this->name . ' exists for ID: ' . $id . '</strong>');
		if ($model)
			$model->reject();
		$this->redirect('/hr/holiday/index');
	}
	
	public function actionCalendar($year=null, $month=null){
		$year = $year ? $year : date('Y');
		$month = $month ? $month : date('n');
		$this->render('calendar/index', array(
			'year' => $year,
			'month' => $month,
		));
	}
	
	public function actionView($id) {
		$this->actionRequest($id);
	}

	public function actionRequest($id) {
		$model = NData::loadModel($this->modelName, $id, '<strong>No ' . $this->name . ' exists for ID: ' . $id . '</strong>');
		$this->render('request', array('model' => $model));
	}

	public function actionDelete($id) {

		$model = NData::loadModel($this->modelName, $id, '<strong>No ' . $this->name . ' exists for ID: ' . $id . '</strong>');
		$model->delete();
		HrHoliday::model()->deleteAllByAttributes(array('request_id'=>$id));

		Yii::app()->user->setFlash('success', 'Successfully deleted request id #'.$id);
		$this->redirect(array('index'));
	}

}