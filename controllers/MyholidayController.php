<?php

class MyholidayController extends HrAdminController {

	public $modelName = 'HrStaffHoliday';
	public $name = 'myholiday';

	public function actionIndex($year=null) {
		$this->render('index', array('year'=>$year));
	}

	public function actionCreate($date=null, $year=null) {
		$model = new $this->modelName;
		$model->staff_id = HrHelper::userStaffRecord()->id;

		if (isset($_POST[$this->modelName])) {
			$dateModelName = 'HrHoliday';
			$request = new HrHolidayRequest;
			$request->staff_id = HrHelper::userStaffRecord()->id;
			$request->state = 0;
			$request->description = $_POST[$this->modelName]['description'];
			$request->year = $_POST[$this->modelName]['year'];
			$request->requested_at = date('Y-m-d H:i:s');
			if ($request->save()) {
				switch($_POST['form_type']) {
					case "simple" :
						$dates = array();
						$firstDate = $_POST[$dateModelName]['first_date'];
						$lastDate = isset($_POST[$dateModelName]['last_date']) ? $_POST[$dateModelName]['last_date'] : '';
						if ($lastDate>'0000-00-00' && $lastDate!=$firstDate) {
							$date = $firstDate;
							$count = $_POST['remaining_original'];
							while ($date <= $lastDate && $count>0) {
								$count--;
								$dates[] = $date;
								$udate = strtotime($date);
								$date = date('Y-m-d', mktime(0, 0, 0, date("m",$udate)  , date("d",$udate)+1, date("Y",$udate)));
								$date = HrHelper::checkDate($date);
							}
						} else {
							$dates[] = $firstDate;
						}
						foreach ($dates as $date) {
							$model = new $dateModelName;
							$model->staff_id = HrHelper::userStaffRecord()->id;
							$model->date = $date;
							$model->duration = 1;
							$model->request_id = $request->id;
							$model->save();
						}
						break;
					case "advanced" :
						foreach ($_POST[$dateModelName]['date'] as $count => $date) {
							$model = new $dateModelName;
							$model->staff_id = HrHelper::userStaffRecord()->id;
							$model->date = $date;
							$model->duration = $_POST[$dateModelName]['duration'][$count];
							$model->half_day_period = $_POST[$dateModelName]['half_day_period'][$count];
							$model->request_id = $request->id;
							$model->save();
						}
						break;
				}
				
				// Send email...
				$staff = $request->staff;
				$manager = $request->staff->manager;
				
				// If you don't have a manager, we assume you *are* a manager...
				if (!$manager)
					$manager = $staff;
				
				$msg  = 'Hi '.$manager->user->first_name.',<br /><br />';
				$msg .= "{$staff->name} has requested holiday for the following date(s):";
				$msg .= "<ul>";
				foreach ($request->requestDates as $date) {
					$msg .= "<li>" . NHtml::formatDate($date['date']) . ($date['period'] ? ' - Half Day ('.$date['period'].')' : '') . "</li>";
				}
				$msg .= "</ul>";
				if ($request->description) {
					$msg .= "Description: <em>" . $request->description . "</em><br /><br />";
				}
				$msg .= "Please ".HrHelper::holidayRequestManagerLink($request, 'review this request here').".";
				$msg .= "<br /><br />If you are happy with the request, you can ".HrHelper::holidayRequestManagerApproveLink($request, 'approve it here').".";
				$msg .= "<br /><br />Thanks!";

				$subject = "Holiday request from {$staff->name} - please review";			

				$message = Yii::app()->email->compose();
				$message->addTo($manager->email, $manager->name)
					->setSubject($subject)
					->setHtml($msg)
					->send();
			}
			$this->redirect(array('index', 'year'=>$request->year));
		}

		$this->render('create', array('model' => $model, 'date' => $date, 'year' => $year));
	}

	public function actionAddDateRow($count, $date=null) {
		if ($date!==null)
			$date = HrHelper::checkDate($date);
		$disabledDates = HrHelper::holidayBookedDates();
		$bankHolidays = HrHelper::bankHolidayDates();

		$this->renderPartial('_date_row', array('count'=>$count, 'date'=>$date, 'disabledDates'=>$disabledDates, 'bankHolidays'=>$bankHolidays), false, true);
	}
	
	public function actionCountDays($fromDate, $toDate='0000-00-00') {
		$dates = array();
		if ($toDate>'0000-00-00' && $toDate!=$fromDate) {
			$date = $fromDate;
			while ($date <= $toDate) {
				$dates[] = $date;
				$udate = strtotime($date);
				$date = date('Y-m-d', mktime(0, 0, 0, date("m",$udate)  , date("d",$udate)+1, date("Y",$udate)));
				$date = HrHelper::checkDate($date);
			}
		} else {
			$dates[] = $fromDate;
		}
		echo count($dates);
	}
	
	public function actionRequest($id) {
		$model = NData::loadModel($this->modelName, $id, '<strong>No ' . $this->name . ' exists for ID: ' . $id . '</strong>');
		$this->render('request', array('model' => $model));
	}
	
	public function actionCalendar($year=null){
		$year = $year ? $year : date('Y');
		$this->renderPartial('_calendar', array(
			'year' => $year,
		), false, true);
	}
	
	public function actionDelete($id) {

		$model = NData::loadModel($this->modelName, $id, '<strong>No ' . $this->name . ' exists for ID: ' . $id . '</strong>');
		$model->delete();
		HrHoliday::model()->deleteAllByAttributes(array('request_id'=>$id));

		Yii::app()->user->setFlash('success', 'Successfully deleted request id #'.$id);
		$this->redirect(array('index'));
	}

}