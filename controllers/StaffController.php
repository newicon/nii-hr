<?php

class StaffController extends HrAdminController {

	public $modelName = 'HrStaff';
	public $name = 'staff';
		
	public function actionCreateHoliday($staff){
		$staff = HrStaff::model()->findByPk($staff);
		$model = new HrHoliday;
		$model->staff_id = $staff->id;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model, 'HrHolidayForm');

		if (isset($_POST['HrHoliday'])) {
			$model->attributes = $_POST['HrHoliday'];
			if ($model->save())
				$this->redirect(array('view', 'id' => $staff->id));
		}

		$this->render('holiday/create', array(
			'model' => $model,
			'staff' => $staff,
		));
	}
	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate() {
		$model = new $this->modelName;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model, $this->modelName.'Form');

		if (isset($_POST[$this->modelName])) {
			$model->attributes = $_POST[$this->modelName];
			if ($model->save()) {
				if (isset($_POST['holiday_allowance']))
					$model->updateHolidayAllowance($_POST['holiday_allowance']);
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array('model' => $model));
	}
	
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionEdit($id) {
		$model = NData::loadModel($this->modelName, $id, '<strong>No ' . $this->name . ' exists for ID: ' . $id . '</strong>');

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model, $this->modelName.'Form');

		if (isset($_POST[$this->modelName])) {
			$model->attributes = $_POST[$this->modelName];
			if ($model->save()) {
				if (isset($_POST['holiday_allowance']))
					$model->updateHolidayAllowance($_POST['holiday_allowance']);
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('edit', array('model' => $model));
	}

}