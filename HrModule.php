<?php

class HrModule extends NWebModule {

	public $name = 'HR';
	public $description = 'Module to manage HR';
	public $version = '1.0.0';

	public function init() {
		Yii::import('hr.models.*');
		Yii::import('hr.models.grids.*');
		Yii::import('hr.components.*');
		Yii::import('hr.helpers.*');
	}

	public function setup() {
		Yii::app()->menus->addItem('main', 'HR', '#', null, array('order'=>30, 'icon'=>'icon-folder-open'));
		Yii::app()->menus->addItem('main', 'My Holiday', array('/hr/myholiday'),'HR');
		Yii::app()->menus->addItem('main', 'Staff', array('/hr/staff/index'), 'HR');
		Yii::app()->menus->addItem('main', 'Holiday Requests', array('/hr/holiday/index'), 'HR');
		Yii::app()->menus->addItem('main', 'Holiday Calendar', array('/hr/holiday/calendar'), 'HR');

		Yii::app()->menus->addItem('user', 'Holiday', array('/hr/myholiday'),'User');
	}

	public function permissions() {
		return array(
			'hr' => array('description' => 'HR',
				'tasks' => array(
					'view' => array('description' => 'View HR Details',
						'roles' => array('administrator', 'editor', 'viewer'),
						'operations' => array(
						),
					),
					'edit' => array('description' => 'Edit HR Details',
						'roles' => array('administrator', 'editor'),
						'operations' => array(
						),
					),
				),
			),
		);
	}

	public $sideBarItems = array(
		array('label' => 'Human Resources'),
		array('label' => 'Staff', 'url' => array('/hr/staff/index')),
		array('label' => 'Holiday Requests', 'url' => array('/hr/holiday/index')),
		array('label' => 'Holiday Calendar', 'url' => array('/hr/holiday/calendar')),
	);

	public function install() {
		HrBankHoliday::install('HrBankHoliday');
		HrBankHoliday::model()->insertDefaultData();
		HrHoliday::install('HrHoliday');
		HrStaff::install('HrStaff');
		HrHolidayRequest::install('HrHolidayRequest');
		HrStaffHolidayAllowance::install('HrStaffHolidayAllowance');
		
		$this->installPermissions();
	}

}
