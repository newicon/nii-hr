<?php

class HrStaffHoliday extends HrHolidayRequest {
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return Contact the static model class
	 */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	
	public function defaultScope(){
		$conditions=array();
		if ($this->staff_id)
			$conditions[] = 'staff_id = "'.$this->staff_id.'"';
		if ($this->year)
			$conditions[] = 'year = "'.$this->year.'"';
		return array(
			'condition' => implode(' AND ', $conditions),
		);
	}

}
