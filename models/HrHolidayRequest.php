<?php

class HrHolidayRequest extends NActiveRecord {
	
	const REQUESTED = 0;
	const APPROVED = 1;
	const REJECTED = 2;

	/**
	 * Returns the static model of the specified AR class.
	 * @return Contact the static model class
	 */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return '{{hr_holiday_request}}';
	}

	public function getModule() {
		return Yii::app()->getModule('hr');
	}
	
	public function relations(){
		return array(
			'staff' => array(self::BELONGS_TO, 'HrStaff', 'staff_id'),
			'dates' => array(self::HAS_MANY, 'HrHoliday', 'request_id'),
			'updated_by' => array(self::BELONGS_TO, 'HrStaff', 'updated_by_staff_id'),
		);
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('requested_at,staff_id,year','required'),
			array('state','safe'),
			array('requested_at,staff_id,state,year','safe','on'=>'search'),
		);
	}
	
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return NActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {

		$criteria = $this->getDbCriteria();
		
		$criteria->compare('id', $this->id);
		$criteria->compare('staff_id', $this->staff_id);
		$criteria->compare('requested_at', $this->requested_at, true);
		$criteria->compare('state', $this->state);
		$criteria->compare('year', $this->year);
		
		$criteria->order = 'requested_at ASC';

		return new NActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
	
	public function stateLabels(){
		return array(
			self::REQUESTED => 'Requested',
			self::APPROVED => 'Approved',
			self::REJECTED => 'Rejected',
		);
	}
	
	public function getStateLabel(){
		$stateLabels = $this->stateLabels();
		return isset($stateLabels[$this->state]) ? $stateLabels[$this->state] : 'N/A';
	}

	public function approve(){
		$this->state = self::APPROVED;
		$this->state_updated_at = date('Y-m-d H:i:s');
		$this->updated_by_staff_id = HrHelper::userStaffRecord()->id;
		$this->save(false,array('state', 'state_updated_at', 'updated_by_staff_id'));
		HrHoliday::model()->updateAll(array('state'=>HrHoliday::APPROVED), 'request_id = '.$this->id);
		
		// Send email...
		$staff = $this->staff;
		$manager = $this->staff->manager;
		$msg  = 'Hi '.$staff->user->first_name.',<br /><br />';
		$msg .= "Good news! {$manager->name} has APPROVED your requested holiday for the following date(s):";
		$msg .= "<ul>";
		foreach ($this->requestDates as $date)
			$msg .= "<li>" . NHtml::formatDate($date['date']) . ($date['period'] ? ' - Half Day ('.$date['period'].')' : '') . "</li>";
		$msg .= "</ul>";
		if ($this->description)
			$msg .= "Description: <em>" . $this->description . "</em><br /><br />";

		$subject = "Holiday request #{$this->id} - APPROVED";			

		$message = Yii::app()->email->compose();
		$message->addTo($staff->email, $staff->name)
			->setSubject($subject)
			->setHtml($msg)
			->send();
	}
	
	public function reject(){
		$this->state = self::REJECTED;
		$this->state_updated_at = date('Y-m-d H:i:s');
		$this->updated_by_staff_id = HrHelper::userStaffRecord()->id;
		if (isset($_POST['reject_reason']))
			$this->state_reason = $_POST['reject_reason'];
		$this->save(false,array('state', 'state_updated_at', 'state_reason', 'updated_by_staff_id'));
		HrHoliday::model()->updateAll(array('state'=>HrHoliday::REJECTED), 'request_id = '.$this->id);
		
		// Send email...
		$staff = $this->staff;
		$manager = $this->staff->manager;
		$msg  = 'Hi '.$staff->user->first_name.',<br /><br />';
		$msg .= "Sorry, {$manager->name} has REJECTED your requested holiday for the following date(s):";
		$msg .= "<ul>";
		foreach ($this->requestDates as $date)
			$msg .= "<li>" . NHtml::formatDate($date['date']) . ($date['period'] ? ' - Half Day ('.$date['period'].')' : '') . "</li>";
		$msg .= "</ul>";
		if ($this->description)
			$msg .= "Description: <em>" . $this->description . "</em><br /><br />";
		if ($this->state_reason)
			$msg .= "The reason given was:<em>" . $this->state_reason . "</em><br /><br />";
		$msg .= "You can review this request " . HrHelper::holidayRequestViewLink($this, 'here') .".";

		$subject = "Holiday request #{$this->id} - REJECTED";			

		$message = Yii::app()->email->compose();
		$message->addTo($staff->email, $staff->name)
			->setSubject($subject)
			->setHtml($msg)
			->send();
	}
	
	public function scopes(){
		return array(
			'requested' => array('condition'=>'state='.self::REQUESTED),
			'approved' => array('condition'=>'state='.self::APPROVED),
			'rejected' => array('condition'=>'state='.self::REJECTED),
		);
	}
	
	public function byStaff($staff_id=null){
		$staff_id = $staff_id ? $staff_id : $this->staff_id;
		$this->getDbCriteria()->addCondition('staff_id='.$staff_id);
	}

	public function getRequestDates(){
		$dates=array();
		foreach ($this->dates as $date) {
			$count = HrHelper::countBookedForDate($date->date, $this->staff_id);
			$dates[$date->date] = array('date'=>$date->date, 'bookedCount'=>$count, 'labelClass'=>HrHelper::countLabelClass($count), 'period'=>$date->half_day_period);
		}
		return $dates;
	}

	public function getRequestDatesFormatted() {
		$dates=array();
		foreach ($this->dates as $date)
			$dates[] = NHtml::formatDate($date->date);
		return $dates;
	}

	public function getRequestDuration(){
		$duration=0;
		foreach ($this->dates as $date)
			$duration += (float) $date->duration;
		return $duration . ' day' . ($duration==1?'':'s');
	}

	public function getRemainingHoliday($year=null) {
		$remaining = $this->staff->getRemainingHoliday($year);
		return $remaining . ' day' . ($remaining==1?'':'s');
	}
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'staff_id' => 'Staff',
			'requested_at' => 'Requested Date',
			'state' => 'State',
		);
	}

	public function schema() {
		return array(
			'columns' => array(
				'id' => 'pk',
				'staff_id' => 'int',
				'requested_at' => 'datetime',
				'description' => 'varchar(255)',
				'state' => 'int NOT NULL DEFAULT \''.self::REQUESTED.'\'',
				'state_reason' => 'varchar(255)',
				'state_updated_at' => 'datetime',
				'updated_by_staff_id' => 'int',
				'year' => 'char(4)',
			),
			'keys' => array()
		);
	}
	
}