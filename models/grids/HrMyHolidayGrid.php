<?php

class HrMyHolidayGrid extends HrStaffHoliday {
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return Contact the static model class
	 */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	
	public function defaultScope(){$yearCondition='';
		if ($this->year)
			$yearCondition = ' AND year = "'.$this->year.'"';
		return array(
			'condition' => 'staff_id='.HrHelper::userStaffRecord()->id . $yearCondition,
		);
	}
	
}
