<?php

class HrStaff extends NActiveRecord {

	/**
	 * Returns the static model of the specified AR class.
	 * @return Contact the static model class
	 */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return '{{hr_staff}}';
	}

	public function defaultScope()
	{
		return 'employed';
	}

	public function scopes()
	{
		return [
			'employed' =>[
				'condition'=>'employed = 1',
			]
		];
	}

	public static function getDepartments()
	{
		$data = self::model()->cmd()->select('department')->group('department')->queryColumn();
		$ret = [];
		foreach($data as $d) $ret[$d] = $d;
		if (!isset($ret[''])) $ret[''] = '';
		return $ret;
	}

	public function getModule() {
		return Yii::app()->getModule('hr');
	}

	public function relations(){
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'manager' => array(self::BELONGS_TO, 'HrStaff', 'manager_id'),
		);
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id,holiday_allowance','required'),
			array('user_id','unique','message'=>'This user is already a staff member'),
			array('job_title,manager_id,department,employed','safe'),
			array('user_id,job_title,manager_id,department','safe','on'=>'search'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return NActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		$criteria = $this->getDbCriteria();
		$criteria->addCondition('employed = 1');
		$criteria->compare('id', $this->id);
		$criteria->compare('job_title', $this->job_title, true);
		$criteria->compare('manager_id', $this->manager_id);

		return new NActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination'=>array(
				'pageSize'=>100,
            ),
		));
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'user_id' => 'Linked User',
			'job_title' => 'Job Title',
			'manager_id' => 'Manager',
			'department' => 'Department',
			'employed' => 'Employed',
			'holiday_allowance' => 'Holiday Allowance',
		);
	}

	public function updateHolidayAllowance($allowanceArray=array()) {
		foreach ($allowanceArray as $year => $allowance) {
			if ($allowance<=0) {
				HrStaffHolidayAllowance::model()->deleteAllByAttributes(array('staff_id'=>$this->id, 'year'=>$year));
				continue;
			}
			$a = HrStaffHolidayAllowance::model()->findByAttributes(array('staff_id'=>$this->id, 'year'=>$year));
			if (!$a) {
				$a = new HrStaffHolidayAllowance;
				$a->staff_id = $this->id;
				$a->year = $year;
			}
			$a->allowance = $allowance;
			$a->save();
		}
	}

	public function getName(){
		if ($this->user)
			return $this->user->name;
	}

	public function getEmail(){
		if ($this->user)
			return $this->user->email;
	}

	public function getRemainingHoliday($year=null){
		$year = $year ? $year : date('Y');
		return HrHelper::getStaffHolidayAllowance($year, $this) - $this->getTotalHoliday($year.'-01-01',$year.'-12-31');
	}

	public function getTotalHoliday($from=null,$to=null){
		$from = $from ? $from : date('Y').'-01-01';
		$to = $to ? $to : date('Y').'-12-31';
		return Yii::app()->db->createCommand()
					->select('SUM(duration)')
					->from(HrHoliday::model()->tableName())
					->where('staff_id=:staff_id AND `date` BETWEEN :from AND :to AND state<>:state',
							array(':staff_id'=>$this->id, ':from'=>$from, ':to'=>$to, ':state'=> HrHoliday::REJECTED))
					->queryScalar();
	}

	public function schema() {
		return array(
			'columns' => array(
				'id' => 'pk',
				'user_id' => 'int',
				'manager_id' => 'int',
				'department' => 'varchar(255)',
				'employed' => 'tinyint default 1',
				'job_title' => 'varchar(255)',
				'holiday_allowance' => 'decimal(10,2)',
			),
			'keys' => array()
		);
	}

}
