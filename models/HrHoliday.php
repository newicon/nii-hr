<?php

class HrHoliday extends NActiveRecord {
	
	const REQUESTED = 0;
	const APPROVED = 1;
	const REJECTED = 2;

	public $bookedCount;

	/**
	 * Returns the static model of the specified AR class.
	 * @return Contact the static model class
	 */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return '{{hr_holiday}}';
	}

	public function getModule() {
		return Yii::app()->getModule('hr');
	}
	
	public function relations(){
		return array(
			'staff' => array(self::BELONGS_TO, 'HrStaff', 'staff_id'),
		);
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('date,staff_id,duration','required'),
			array('date','dateValidator'),
			array('duration','durationValidator'),
			array('state,half_day_period','safe'),
			array('date,staff_id,duration,half_day_period','safe','on'=>'search'),
		);
	}
	
	public function dateValidator($attribute,$params){
		// Check to see if the date is a weekend
		if(HrHelper::isWeekend($this->date))
            $this->addError('date','You can not book a holiday on a weekend.');
		// Check to see if the date is a bank holiday
		$model = HrBankHoliday::model()->findByAttributes(array('date' => $this->date));
        if($model)
            $this->addError('date','You can not book a holiday on a bank holiday.');
		// Check to see if the date has already been booked by the member of staff
		$model = HrHoliday::model()->findByAttributes(array('date' => $this->date, 'staff_id' => $this->staff_id));
        if($model)
            $this->addError('date','A holiday has already been requested for this date.');
	}
	
	public function durationValidator($attribute,$params){
		$staff = HrStaff::model()->findByPk($this->staff_id);
		if($staff && $staff->getRemainingHoliday(date('Y',  strtotime($this->date))) < $this->duration)
			$this->addError('duration','You do not have enough holiday for this duration.');
	}

	public function getPeriodFormatted() {
		if ($this->duration == '0.5')
			return 'Half Day - ' . $this->half_day_period;
		else
			return 'Full Day';
	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return NActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {

		$criteria = $this->getDbCriteria();

		$criteria->compare('id', $this->id);
		$criteria->compare('staff_id', $this->staff_id);
		$criteria->compare('date', $this->date, true);
		$criteria->compare('duration', $this->duration, true);
		$criteria->compare('state', $this->state);
		
		$criteria->order = 'date ASC';

		return new NActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
	
	public function stateLabels(){
		return array(
			self::REQUESTED => 'Requested',
			self::APPROVED => 'Approved',
			self::REJECTED => 'Rejected',
		);
	}
	
	public function getStateLabel(){
		$stateLabels = $this->stateLabels();
		return isset($stateLabels[$this->state]) ? $stateLabels[$this->state] : 'N/A';
	}
	
	public function scopes(){
		return array(
			'requested' => array('condition'=>'state='.self::REQUESTED),
			'approved' => array('condition'=>'state='.self::APPROVED),
			'rejected' => array('condition'=>'state='.self::REJECTED),
		);
	}
	
	public function byStaff($staff_id=null){
		$staff_id = $staff_id ? $staff_id : $this->staff_id;
		$this->getDbCriteria()->addCondition('staff_id='.$staff_id);
	}
	
	public function dateRange($from,$to){
		$this->getDbCriteria()->addBetweenCondition('date', $from, $to);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'staff_id' => 'Staff',
			'date' => 'Date',
			'duration' => 'Duration',
			'state' => 'State',
		);
	}

	public function schema() {
		return array(
			'columns' => array(
				'id' => 'pk',
				'staff_id' => 'int',
				'date' => 'date',
				'duration' => 'decimal(10,2)',
				'half_day_period' => 'varchar(2)',
				'request_id' => 'int',
				'state' => 'int NOT NULL DEFAULT \''.self::REQUESTED.'\'',
			),
			'keys' => array()
		);
	}
	
}
