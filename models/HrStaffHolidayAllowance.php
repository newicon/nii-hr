<?php

class HrStaffHolidayAllowance extends NActiveRecord {
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return Contact the static model class
	 */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return '{{hr_staff_holiday_allowance}}';
	}

	public function getModule() {
		return Yii::app()->getModule('hr');
	}
	
	public function relations(){
		return array(
			'staff' => array(self::BELONGS_TO, 'HrStaff', 'staff_id'),
		);
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('staff_id,year,allowance','required'),
		);
	}
	
	
	public function schema() {
		return array(
			'columns' => array(
				'staff_id' => 'int',
				'year' => 'char(4)',
				'allowance' => 'decimal(10,2)',
				'PRIMARY KEY (staff_id, year)',
			),
			'keys' => array()
		);
	}

}
