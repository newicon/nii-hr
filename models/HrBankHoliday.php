<?php

class HrBankHoliday extends NActiveRecord {

	/**
	 * Returns the static model of the specified AR class.
	 * @return Contact the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return '{{hr_bank_holiday}}';
	}

	public function getModule() {
		return Yii::app()->getModule('hr');
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('date,description', 'required'),
			array('date,description', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return NActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {

		$criteria = $this->getDbCriteria();

		$criteria->compare('id', $this->id);
		$criteria->compare('date', $this->date, true);
		$criteria->compare('description', $this->description, true);

		return new NActiveDataProvider($this, array(
					'criteria' => $criteria,
				));
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'date' => 'Date',
			'description' => 'Description',
		);
	}

	public function schema() {
		return array(
			'columns' => array(
				'id' => 'pk',
				'date' => 'date',
				'description' => 'varchar(255)',
			),
			'keys' => array()
		);
	}

	public function insertDefaultData() {
		foreach ($this->defaultBankHolidays() as $date => $description) {
			$model = $this->findByAttributes(array('date' => $date));
			if (!$model) {
				$model = new HrBankHoliday;
				$model->date = $date;
				$model->description = $description;
				$model->save();
			}
		}
	}

	public function defaultBankHolidays() {
		// return an array month-day
		return array(
			'2013-01-01' => 'New Year\'s Day',
			'2013-03-29' => 'Good Friday',
			'2013-04-01' => 'Easter Monday',
			'2013-05-06' => 'May Day Bank Holiday',
			'2013-05-27' => 'Spring Bank Holiday',
			'2013-08-26' => 'Summer Bank Holiday',
			'2013-12-25' => 'Christmas Day',
			'2013-12-26' => 'Boxing Day',
			'2014-01-01' => 'New Year\'s Day',
			'2014-04-18' => 'Good Friday',
			'2014-04-21' => 'Easter Monday',
			'2014-05-05' => 'May Day Bank Holiday',
			'2014-05-26' => 'Spring Bank Holiday',
			'2014-08-25' => 'Summer Bank Holiday',
			'2014-12-25' => 'Christmas Day',
			'2014-12-26' => 'Boxing Day',
			'2015-01-01' => 'New Year\'s Day',
			'2015-04-03' => 'Good Friday',
			'2015-04-06' => 'Easter Monday',
			'2015-05-04' => 'May Day Bank Holiday',
			'2015-05-25' => 'Spring Bank Holiday',
			'2015-08-31' => 'Summer Bank Holiday',
			'2015-12-25' => 'Christmas Day',
			'2015-12-28' => 'Boxing Day',
		);
	}

}
