<div class="btn-group" style="float:right;" id="calendar-month-select">
	<a href="javascript:void(0);" class="btn" data-month-select="previous" id="data-month-select-previous"><i class="icon-chevron-left"></i> Prev</a>
	<a href="javascript:void(0);" class="btn btn-info" data-month-select="current">Current</a>
	<a href="javascript:void(0);" class="btn" data-month-select="next" id="data-month-select-next">Next <i class="icon-chevron-right"></i></a>
</div>
<?php

function createDayCellMonthView($da, $mo, $ye, $hols)
{
	$staff = $hols['staff'];
	$hols = $hols['dates'];
	
	$dayNum   = date("N", mktime(0, 0, 0, $mo, $da, $ye));
	$fullDate = date("Y-m-d", mktime(0, 0, 0, $mo, $da, $ye));
	$count	  = isset($hols[$fullDate]) ? $hols[$fullDate]['count'] : 0;
	
	$type	  = isset($hols[$fullDate]) ? $hols[$fullDate]['type'] : 'blank';
	$comment  = isset($hols[$fullDate]) && $hols[$fullDate]['comment'] != '' ? '*' : ($count > 0 ? $count : '');
	$commentTitle = isset($hols[$fullDate]) && $hols[$fullDate]['comment'] != '' ? '* ' . $hols[$fullDate]['comment'] : ($count > 0 ? $count.' staff with holiday' : '');
	$requestId = isset($hols[$fullDate]) && isset($hols[$fullDate]['request_id']) ? $hols[$fullDate]['request_id'] : '';
	if ($requestId !== '' && isset($hols[$fullDate]['state']))
		$commentTitle = '<span class="label label-'.HrHelper::requestStateLabelClass($hols[$fullDate]['state']).'">' . $type .'</span><br />' . $commentTitle;
	$extras = $fullDate == date('Y-m-d') ? 'today' : '';
	
	$staff = isset($staff[$fullDate]) ? $staff[$fullDate] : array();
	
	if ($dayNum > 5)
		return false;
	print "
		<div class='mCell mCellCount-$count mCellType-$type $extras' rel='tooltip' data-html='true' data-original-title='$commentTitle' count='$count' type='$type' cellDate='$fullDate' requestid='$requestId'>
			<div class='mCellDay'>$da</div>
			<div class='mCellStaff'>".implode('<br />', $staff)."</div>
		</div>";
}

$dowArr = array('1'=>'Monday', '2'=>'Tuesday', '3'=>'Wednesday', '4'=>'Thursday', '5'=>'Friday', '6'=>'Saturday', '7'=>'Sunday');
	
// cycle through months for navigation purposes
for ($mNum = 1; $mNum <= 12; $mNum++) {

	$this->beginWidget('CWidget'); 
	
	$monthName = date("F", mktime(0, 0, 0, $mNum, 1, $thisYear));
	
	print "
	<div class='singleMonthView".($month==$mNum?'':' hide')."' data-month-number='$mNum' id='month-number-$mNum'>
		<div class='monthName'><h2 class='mtn'>$monthName</h2></div>
		<div class='dowHeader'>";

	// create day of week headers
	foreach ($dowArr as $index => $dow) {
		if ((int)$index >= 6)
			continue;
		print "<div class='dow'><span>$dow</span></div>";
	}

	print "
		</div>
	<div class='mCells'>";

	// week day number of first day of month
	$mDayOne = date("N", mktime(0, 0, 0, $mNum, 1, $thisYear));

	// create empty cells 
	if ($mDayOne <= 5) {
		for ($colDay = 1; $mDayOne > $colDay; $colDay++) {
			print "<div class='mCell blankDate' cellDate='test'></div>";
		}
	}	


	// write numbered day cells
	for ($day = 1; checkDate($mNum, $day, $thisYear); $day++) {
		print createDayCellMonthView($day, $mNum, $thisYear, $hols);
	}


	print "
		</div>
	</div>";

	$this->endWidget(); 
}
?>
<input type="hidden" id="current-visible-month" value="<?php echo $month; ?>" />
<script type="text/javascript">
	var durClasses = $.parseJSON('<?php $durFmt = TimesheetHoliday::durClasses(); echo CJSON::encode($durFmt); ?>');
	$('.mCell').tooltip();

	jQuery(function($){
		var currentmonth = <?php echo $month; ?>;
		$('#calendar-month-select').on('click', 'a.btn', function(e){
			if ($(e.currentTarget).attr('disabled'))
				return false;
			var monthselect = $(e.currentTarget).attr('data-month-select');
			$('.singleMonthView').hide();
			var selectedmonth;
			var currentvisible = parseInt($('#current-visible-month').val());
			switch(monthselect) {
				case "previous" :
					selectedmonth = currentvisible - 1;
					break;
				case "next" :
					selectedmonth = currentvisible + 1;
					break;
				case "current" :
					selectedmonth = currentmonth;
					break;
			}
			if (selectedmonth==1)
				$('#data-month-select-previous').attr('disabled', 'disabled');
			else
				$('#data-month-select-previous').removeAttr('disabled');
			if (selectedmonth==12)
				$('#data-month-select-next').attr('disabled', 'disabled');
			else
				$('#data-month-select-next').removeAttr('disabled');
			$('#month-number-'+selectedmonth).show();
			$('#current-visible-month').val(selectedmonth);
		});
	});
	
	<?php if ($staff_id!==null) { 
		$staff = HrStaff::model()->findByPk($staff_id); ?>
		jQuery(function($){
			var remaining = <?php echo $staff->remainingHoliday; ?>;
			$('.mCellType-blank').on('click', function(e){
				if (remaining==0) {
					alert("Sorry! You don't have enough allowance for any extra holiday");
					return false;
				}
				if (!confirm('Would you like to request a holiday for this date?'))
					return false;
				var date = $(e.currentTarget).attr('cellDate');
				window.location = '<?php echo NHtml::url(array('/hr/myholiday/create')); ?>/date/'+date;
			});
			$('.mCell').on('click', function(e){
				if (!$(e.currentTarget).attr('requestid'))
					return false;
				window.location = '<?php echo NHtml::url(array('/hr/myholiday/request')); ?>/id/'+$(e.currentTarget).attr('requestid');
			});
		});
	<?php } ?>

</script>