<?php

$curMonthNum = date('n');
$curYear	 = date('Y');

function createDayCell($da, $mo, $ye, $hols)
{
	$staff = $hols['staff'];
	$hols = $hols['dates'];
	$dayNum   = date("N", mktime(0, 0, 0, $mo, $da, $ye));
	$fullDate = date("Y-m-d", mktime(0, 0, 0, $mo, $da, $ye));
	$count	  = isset($hols[$fullDate]) ? $hols[$fullDate]['count'] : 0;
	
	$type	  = isset($hols[$fullDate]) ? $hols[$fullDate]['type'] : 'blank';
	$comment  = isset($hols[$fullDate]) && $hols[$fullDate]['comment'] != '' ? '*' : ($count > 0 ? $count : '');
	$commentTitle = isset($hols[$fullDate]) && $hols[$fullDate]['comment'] != '' ? '* ' . $hols[$fullDate]['comment'] : ($count > 0 ? $count.' staff with holiday' : '');
	$requestId = isset($hols[$fullDate]) && isset($hols[$fullDate]['request_id']) ? $hols[$fullDate]['request_id'] : '';
	if ($requestId !== '' && isset($hols[$fullDate]['state']))
		$commentTitle = '<span class="label label-'.HrHelper::requestStateLabelClass($hols[$fullDate]['state']).'">' . $type .'</span><br />' . $commentTitle;
	$extras = $fullDate == date('Y-m-d') ? 'today' : '';
	
	if ($dayNum > 5)
		print "<div class='ymCell notDate'></div>";
	else
		print "
		<div class='ymCell ymCellCount-$count ymCellType-$type $extras' rel='tooltip' data-html='true' data-original-title='$commentTitle' count='$count' type='$type' cellDate='$fullDate' requestid='$requestId'>
			<span class='ymCellDay'>$da</span>
			<span class='ymCellComment'>$comment</span>
		</div>";
}

// cycle through months and add widget
for ($mNum = 1; $mNum <= 12; $mNum++) 
{
	$this->beginWidget('CWidget'); 
	
	$mName  = date('F', mktime(0, 0, 0, $mNum, 1));
	$dowArr = array('M','T','W','T','F','S','S');
	$curMon = ($mNum == $curMonthNum && $thisYear == $curYear) ? 'curMonth' : '';
	
	print "
	<div class='monthWidget' month='$mNum'>
		<div class='mHeader $curMon' data-month-select='$mNum'>$mName</div>
		<div class='dowHeader'>";
			
	// create day of week single letter headers
	foreach ($dowArr as $dow)
	{
		$dowClass = $dow === 'S' ? 'dowHidden' : '';
		$dow	  = $dow === 'S' ? '' : $dow;
		print "<div class='dow $dowClass'><span>$dow</span></div>";
	}
		
	print "
		</div>
	<div class='ymCells'>";
	
	// week day number of first day of month
	$mDayOne = date("N", mktime(0, 0, 0, $mNum, 1, $thisYear));

	if ($mDayOne <= 5) // create empty cells 
	{
		for ($colDay = 1; $mDayOne > $colDay; $colDay++)
		{
			print "<div class='ymCell blankDate' cellDate='test'></div>";
		}
	}	
	
				
	// write numbered day cells
	for ($day = 1; checkDate($mNum, $day, $thisYear); $day++)
	{
		print createDayCell($day, $mNum, $thisYear, $hols);
	}
	
	
	print "
		</div>
	</div>";
	
	$this->endWidget(); 
}
?>
<script type="text/javascript">
	var durClasses = $.parseJSON('<?php $durFmt = TimesheetHoliday::durClasses(); echo CJSON::encode($durFmt); ?>');
	$('.ymCell').tooltip();

	<?php if ($staff_id!==null) { 
		$staff = HrStaff::model()->findByPk($staff_id); ?>
		jQuery(function($){
			var remaining = <?php echo $staff->remainingHoliday; ?>;
			$('.ymCellType-blank').on('click', function(e){
				if (remaining==0) {
					alert("Sorry! You don't have enough allowance for any extra holiday");
					return false;
				}
				if (!confirm('Would you like to request a holiday for this date?'))
					return false;
				var date = $(e.currentTarget).attr('cellDate');
				window.location = '<?php echo NHtml::url(array('/hr/myholiday/create')); ?>/date/'+date;
			});
			$('.ymCell').on('click', function(e){
				if (!$(e.currentTarget).attr('requestid'))
					return false;
				window.location = '<?php echo NHtml::url(array('/hr/myholiday/request')); ?>/id/'+$(e.currentTarget).attr('requestid');
			});
		});
	<?php } ?>

</script>