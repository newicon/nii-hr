<?php

class HolidayCalendar extends CInputWidget {
	
	public $yearvar = 'year';
	public $assetUrl;
	public $registerAssets = true;
	public $staff_id = null;
	public $year = null;
	public $view = 'year';
	public $month;
	
	public function init(){
		if($this->registerAssets)
			$this->registerAssets();
	}
	
	public function registerAssets(){
		$this->assetUrl = Yii::app()->assetManager->publish(dirname(__FILE__) . DIRECTORY_SEPARATOR .'assets');
		Yii::app()->clientScript->registerCssFile($this->assetUrl . '/css/holiday.css');
	}
	
	public function run(){
		$thisYear = isset($_GET[$this->yearvar]) ? $_GET[$this->yearvar] : $this->year ? $this->year : date('Y');
		$month = $this->month ? $this->month : date('n');
		
		$includeStaffInfo = $this->view=='month' ? true : false;
		
		if ($this->staff_id!==null)
			$hols = HrHelper::getUserHolidaysYear($thisYear, $this->staff_id, $includeStaffInfo);
		else
			$hols = HrHelper::getAllUserHolidaysYear($thisYear, $includeStaffInfo);

		$this->render('holidaycalendar_'.$this->view,array(
			'hols' => $hols,
			'thisYear' => $thisYear,
			'staff_id' => $this->staff_id,
			'month' => $month,
		));
	}
}