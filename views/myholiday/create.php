<?php
$year = isset($year) && $year!==null ? $year : (isset($date) ? substr($date, 0, 4) : date('Y'));
// Setup Page elements
$this->title = 'Request a Holiday - ' . $year;
$this->pageTitle = Yii::app()->name . ' - ' . $this->title;
$this->breadcrumbs = array(
	'My Holiday - ' . $year => array('/hr/myholiday', 'year'=>$year),
	'Request a Holiday',
);
?>
<div class="row-fluid">
    <div class="span2">
		<?php $this->renderPartial('_sidebar'); ?>
    </div>
	<div class="span10">
		<?php $this->widget('admin.widgets.Header'); ?>
		<?php $this->renderPartial('_form', array('model' => $model, 'date' => $date, 'year' => $year)); ?>
	</div>
</div>