<div class="well" style="margin-top:15px;padding:8px 0">
	<?php
	$this->widget('bootstrap.widgets.TbMenu', array(
		'type' => 'list',
		'items' => array(
			array('label' => 'My HR'),
			array('label' => 'My Holiday', 'url' => array('/hr/myholiday'), 'active' => true),
			array('label' => 'My Timesheet', 'url' => array('/project/timesheet/calendar')),
		),
	));
	?>
</div>