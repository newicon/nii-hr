<?php
// Setup Page elements
$this->title = 'Holiday Request';
$this->pageTitle = Yii::app()->name . ' - ' . $this->title;
$this->breadcrumbs = array(
	'My Holiday' => array('/hr/myholiday'),
	'Request Details',
);
$actionsHtml = '<span style="font-size:1.3em;line-height:1.3em;" class="mrl label label-'.HrHelper::requestStateLabelClass($model->state).'">'.$model->stateLabel.'</span>';
?>
<div class="row-fluid">
    <div class="span2">
		<?php $this->renderPartial('_sidebar'); ?>
    </div>
	<div class="span10">
		<?php $this->widget('admin.widgets.Header', array('actions'=>$actionsHtml)); ?>
		<div class="well" style="width:800px;">
			<div class="row-fluid detailRow">
				<div class="span3 detailLabel"><?php echo Nii::t('Requested Date(s)') ?></div>
				<div class="span9">
					<table class="table table-striped">
						<thead>
							<tr>
								<th><strong>Date</strong></th>
								<th><strong>Current Booked By</strong></th>
							<tr>
						</thead>
						<tbody>
							<?php foreach ($model->requestDates as $date) { ?>
								<tr>
									<td><?php echo NHtml::formatDate($date['date']) . ($date['period'] ? ' - Half Day ('.$date['period'].')' : '') ?></td>
									<td><?php echo '<span class="label label-'.$date['labelClass'].'">'.$date['bookedCount'].'</span> other staff member' . ($date['bookedCount']==1?'':'s') ?></td>
								</tr>
							<?php 
								$year = substr($date['date'], 0, 4);
							} ?>
						</tbody>
					</table>
				</div>
			</div>
			<div class="row-fluid detailRow">
				<div class="span3 detailLabel"><?php echo Nii::t('Description') ?></div>
				<div class="span9"><?php echo $model->description; ?></div>
			</div>
			<div class="row-fluid detailRow">
				<div class="span3 detailLabel"><?php echo Nii::t('Total Duration') ?></div>
				<div class="span9"><?php echo $model->requestDuration; ?></div>
			</div>
			<div class="row-fluid detailRow">
				<div class="span3 detailLabel"><?php echo Nii::t('Remaining Allowance') ?></div>
				<div class="span9"><?php echo $model->getRemainingHoliday($year) . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;('.$year.' allowance)'; ?></div>
			</div>
			<div class="row-fluid detailRow">
				<div class="span3 detailLabel"><?php echo Nii::t('Requested On') ?></div>
				<div class="span9"><?php echo NHtml::formatDate($model->requested_at, 'jS F Y, H:i'); ?></div>
			</div>
			<?php 
				if ($model->state == $model::REJECTED) {
					$dateLabel = Nii::t('Rejected On');
					$staffLabel = Nii::t('Rejected By');
				} else if ($model->state == $model::APPROVED) {
					$dateLabel = Nii::t('Approved On');
					$staffLabel = Nii::t('Approved By');
				}
				$staff = HrStaff::model()->findByPk($model->updated_by_staff_id);

				if ($model->state_updated_at && isset($dateLabel)) { 
				?>
				<div class="row-fluid detailRow">
					<div class="span3 detailLabel"><?php echo $dateLabel ?></div>
					<div class="span9"><?php echo NHtml::formatDate($model->state_updated_at, 'jS F Y, H:i'); ?></div>
				</div>
				<div class="row-fluid detailRow">
					<div class="span3 detailLabel"><?php echo $staffLabel ?></div>
					<div class="span9"><?php echo HrHelper::staffViewLink($staff) ?></div>
				</div>
			<?php } ?>
			<?php if ($model->state == $model::REJECTED) { ?>
				<div class="row-fluid detailRow">
					<div class="span3 detailLabel">Reason for Rejection</div>
					<div class="span9"><?php echo $model->state_reason ? $model->state_reason : '<span class="noData">--- None given ---'; ?></div>
				</div>
			<?php } ?>
		</div>
		<?php if ($model->staff_id == HrHelper::userStaffRecord()->id) { ?>
			<div class="actions">
				<?php echo NHtml::link('Delete this request', array('/hr/myholiday/delete', 'id'=>$model->id), array('class'=>'text-error')); ?>
			</div>
		<?php } ?>
	</div>
</div>