<?php
$modelName = 'HrMyHolidayGrid';
$model = new $modelName('search');
$model->unsetAttributes();

if (isset($_GET[$modelName]))
	$model->attributes = $_GET[$modelName];

$model->year = $year;

$dataProvider = $model->search(HrHelper::userStaffRecord()->id, $year);
$dataProvider->pagination->pageSize = 5;

$this->widget('nii.widgets.grid.NGridView', array(
	'dataProvider' => $dataProvider,
	'filter' => $model,
	'id' => 'HrStaffHolidayGrid',
	'columns' => array(
		array(
			'name'=>'dates',
			'type'=>'raw',
			'filter'=>false,
			'value'=>'HrHelper::holidayRequestViewLink($data)',
		),
		array(
			'name'=>'duration',
			'type'=>'raw',
			'filter'=>false,
			'value'=>'$data->requestDuration',
			'htmlOptions'=>array('width'=>70),
		),
		array(
			'name' => 'state',
			'value' => '$data->stateLabel',
			'filter'=> HrHelper::requestStateDropdown(),
		),
		array(
			'name' => 'requested_at',
			'type' => 'raw',
			'value' => 'NHtml::formatDate($data->requested_at)',
		),
		array(
			'class' => 'NButtonColumn',
			'headerHtmlOptions' => array('width' => 20),
			'htmlOptions' => array('width' => 20),
			'deleteButtonOptions' => array('class' => 'icon-trash', 'title' => 'Trash'),
			'deleteButtonLabel' => '',
			'deleteButtonImageUrl' => false,
			'deleteButtonUrl' => 'Yii::app()->controller->createUrl("/hr/holiday/delete",array("id"=>$data->primaryKey))',
			'template' => '{delete}',
			'buttons' => array(
				'delete' => array(
					'visible' => '$data->state == HrHoliday::REQUESTED',
				)
//				'approve' => array(
//					'label' => '',
//					'options' => array('class' => 'icon-thumbs-up', 'title' => 'Approve'),
//					'url' => 'Yii::app()->controller->createUrl("/hr/holiday/approve",array("id"=>$data->primaryKey))',
//					'click' => $click,
//				),
//				'reject' => array(
//					'label' => '',
//					'options' => array('class' => 'icon-thumbs-down', 'title' => 'Reject'),
//					'url' => 'Yii::app()->controller->createUrl("/hr/holiday/reject",array("id"=>$data->primaryKey))',
//					'click' => $click,
//				),
			),
		),
	),
));
?>