<?php
$year = isset($year) && $year!==null ? $year : (isset($date) ? substr($date, 0, 4) : date('Y'));
$disabledDates = HrHelper::holidayBookedDates();
$bankHolidays = HrHelper::bankHolidayDates();
$formAction = ($model->id) ? array('edit', 'id' => $model->id) : array('create');
$form = $this->beginWidget('NActiveForm', array(
	'id' => 'HrHolidayForm',
	'action' => $formAction,
	'enableAjaxValidation' => false,
	'enableClientValidation' => true,
	'htmlOptions' => array('class' => 'form-horizontal')
));
$staff = HrStaff::model()->findByAttributes(array('user_id'=>Yii::app()->user->record->id));
$allowance = HrHelper::getStaffHolidayAllowance($year, $staff) * 1;
$remaining = $staff->getRemainingHoliday($year);
$defaultDate = $year != date('Y') ? date('Y-m-d', mktime(0,0,0,1,1,$year)) : date('Y-m-d');
$minDate = date('Y-m-d', mktime(0,0,0,1,1,$year));
$minDateLast = $date !==null ? $date : date('Y-m-d', mktime(0,0,0,1,1,$year));
$maxDate = date('Y-m-d', mktime(0,0,0,12,31,$year));
?>
<div class="well">
	<div class="row-fluid">
		<dl class="span4 dl-horizontal mvn">
			<dt>Holiday Entitlement</dt>
			<dd><?php echo $allowance; ?> day<?php echo $allowance==1?'':'s' ?></dd>
		</dl>
		<dl class="span8 dl-horizontal mvn">
			<dt>Remaining Holiday</dt>
			<dd><span id="remaining_updated"><?php echo $remaining; ?></span> day<?php echo $remaining==1?'':'s' ?></dd>
		</dl>
	</div>
</div>
<input type="hidden" id="remaining_original" name="remaining_original" value="<?php echo $remaining; ?>" />
<div class="tabbable"> <!-- Only required for left/right tabs -->
	<ul class="nav nav-tabs">
		<li class="active"><a href="#simple-tab" data-toggle="tab" data-form="simple">Simple</a></li>
		<li><a href="#advanced-tab" data-toggle="tab" data-form="advanced">Advanced</a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="simple-tab">
			<p class="alert alert-info">
				Select the first and last days of your holiday or use the advanced tab for more complex selection.<br/>
				<em>Hint: For a single day's holiday, simply select the first date.</em>
			</p>
			<div class="control-group row-fluid">
				<div class="span5">
					<div class="control-label">First Day</div>
					<div class="controls">
						<?php echo $this->widget('nii.widgets.forms.DateInput', array('name'=>'HrHoliday[first_date]', 'value'=>$date, 'disabledDates'=>$disabledDates, 'bankHolidays'=>$bankHolidays, 'options'=> array('onClose'=>'js:function( selectedDate ) {$( "#HrHoliday_last_date" ).datepicker( "option", "minDate", selectedDate );countDurationRange( selectedDate , nii.dateToMysql($( "#HrHoliday_last_date" ).datepicker( "getDate" )));}', 'defaultDate'=>$defaultDate, 'minDate'=>$minDate, 'maxDate'=>$maxDate)), true); ?>
					</div>
				</div>
				<div class="span7">
					<div class="control-label" style="width:80px;">Last Day</div>
					<div class="controls" style="margin-left:100px;">
						<?php echo $this->widget('nii.widgets.forms.DateInput', array('name'=>'HrHoliday[last_date]', 'value'=>$date, 'disabledDates'=>$disabledDates, 'bankHolidays'=>$bankHolidays, 'options'=> array('onClose'=>'js:function( selectedDate ) {$( "#HrHoliday_first_date" ).datepicker( "option", "maxDate", selectedDate ); countDurationRange( nii.dateToMysql($( "#HrHoliday_first_date" ).datepicker( "getDate" )), selectedDate );}', 'defaultDate'=>$defaultDate, 'minDate'=>$minDateLast, 'maxDate'=>$maxDate)), true); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="advanced-tab">
			<div id="date-rows" style="width:750px;">
				<?php $this->renderPartial('_date_row', array('count'=>1, 'date'=>$date, 'disabledDates'=>$disabledDates, 'bankHolidays'=>$bankHolidays)); ?>
			</div>
			<div class="control-group">
				<div class="controls">
					<a class="btn" id="add-date-row" href="javascript:void(0);"><i class="icon-plus"></i> Add Day</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php echo $form->field($model, 'description', 'textField'); ?>
<?php 
$model->year = $year;
echo $form->hiddenField($model, 'year'); 
?>
<input type="hidden" id="form_type" name="form_type" value="simple" />
<div class="form-actions">
	<?php
	$cancelUrl = array('/hr/myholiday');
	echo NHtml::submitButton('Save', array('class' => 'btn btn-primary')) . '&nbsp;';
	echo NHtml::btnLink('Cancel', $cancelUrl, null, array('class' => 'btn cancel')) . '&nbsp;';
	?>		
</div>
<?php $this->endWidget(); ?>
<script type="text/javascript">
	
	function countDurationRange(fromDate, toDate) {
		if (fromDate==null || toDate==null || fromDate==false || toDate==false)
			return false;
		$.get('<?php echo NHtml::url(array('/hr/myholiday/countDays')); ?>', {'fromDate':fromDate, 'toDate':toDate}, function(duration){
			var remaining = parseFloat($('#remaining_original').val())-duration;
			if (remaining<0) {
				alert("Warning! You don't have enough allowance for your request.\nAny days over will be excluded from your request");
			}
			$('#remaining_updated').text(remaining);
		});
	}
	
	function countDuration() {
		var duration = 0;
		var max = 0;
		$.each($('.date-row'), function(i, r){
			var rowCount = $(r).attr('data-row-count');
			duration += parseFloat($('#HrHoliday_duration_'+rowCount).val());
			if (rowCount>1) max = rowCount;
		});
		var remaining = parseFloat($('#remaining_original').val())-duration;
		$('#remaining_updated').text(remaining);
		if (remaining==-0.5) {
			alert("Warning: You only have enough allowance for a half day. Please select AM or PM...");
			$('.nav-tabs a[href="#advanced-tab"]').tab('show');
			$('.period-dropdown').focus();
			$('#remaining_updated').text(0);
			$('#form_type').val('advanced');
		} else if (remaining<0) {
			alert("Warning! You don't have enough allowance for any extra days");
			if (max==0) {
				window.location = '<?php echo NHtml::url(array('/hr/myholiday')); ?>';
				return;
			} else {
				$('#delete-day-'+(max-1)).show();
				$('#date-row-'+max).remove();
				countDuration();
			}
		}
	}
	
	$(function(){
		
		countDuration();
		
		$('.nav-tabs').on('click', 'li a', function(e){
			$('#form_type').val($(e.currentTarget).attr('data-form'));
		});
		
		$('#add-date-row').on('click', function(e){
			var count = $('.date-row').size();
			var newcount = parseInt(count) + 1;
			var date = $('#HrHoliday_date_'+count).val();
			if (date!=null && date!='') {
				var d = new Date(date);
				d.setDate(d.getDate() + 1);
				var yyyy = d.getFullYear().toString();
				var mm = (d.getMonth()+1).toString(); // getMonth() is zero-based
				var dd  = d.getDate().toString();
				var newdate = yyyy + '-' + (mm[1]?mm:"0"+mm[0]) + '-' + (dd[1]?dd:"0"+dd[0]);
			} else {
				newdate = null;
			}
			$.get('<?php echo NHtml::url(array('/hr/myholiday/addDateRow')); ?>', {count:newcount,date:newdate}, function(response){
				$('.delete-day').hide();
				$('#date-rows').append(response);
				countDuration();
			});
		});
		
		$('#date-rows').on('change', '.period-dropdown', function(e){
			var count = $(this).attr('data-count');
			var period = $(this).val();
			if (period=='full') {
				$('#HrHoliday_duration_'+count).val('1');
				$('#HrHoliday_half_day_period_'+count).val('');
			} else {
				$('#HrHoliday_duration_'+count).val('0.5');
				$('#HrHoliday_half_day_period_'+count).val(period);
			}
			countDuration();
		});

		$('#date-rows').on('click', '.delete-day', function(e){
			var count = parseInt($(e.currentTarget).attr('data-count'));
			$('#delete-day-'+(count-1)).show();
			$('#date-row-'+count).remove();
			countDuration();
		});
	});
</script>