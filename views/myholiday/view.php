<?php
// Setup Page elements
$this->title = $model->date;
$this->pageTitle = Yii::app()->name . ' - ' . $this->title;
$this->breadcrumbs = array(
	'Holidays' => array('/hr/holiday'),
	$model->date,
);
$this->actionsMenu = array(
	array(
		'icon' => 'icon-pencil',
		'label' => 'Edit',
		'url' => array('edit', 'id' => $model->id),
	),
);
?>
<div class="row-fluid">
    <div class="span2">
		<?php $this->renderPartial('_sidebar'); ?>
    </div>
	<div class="span10">
		<?php $this->widget('admin.widgets.Header'); ?>
		<div class="well" style="width:900px;">
			<div class="line detailRow">
				<div class="unit size1of6 detailLabel"><?php echo Nii::t('Name') ?></div>
				<div class="lastUnit item-title"><?php echo $model->date; ?></div>
			</div>
		</div>
	</div>
</div>