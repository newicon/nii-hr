<?php
$year = isset($year) ? $year : date('Y');
$month = isset($month) ? $month : date('n');
// Setup Page elements
$this->title = 'My Holiday - ' . $year;
$this->pageTitle = Yii::app()->name . ' - ' . $this->title;
$this->breadcrumbs = array(
	'My Holiday - ' . $year,
);
$this->actionsMenu = array(
	array(
		'icon' => 'icon-plus',
		'label' => 'Request a Holiday',
		'url' => array('create', 'year'=>$year),
	),
);
$staff = HrHelper::userStaffRecord();
$allowance = HrHelper::getStaffHolidayAllowance($year, $staff) * 1;
$remaining = $staff->getRemainingHoliday($year);
?>
<div class="row-fluid" id="my-holiday">
    <div class="span2">
		<?php $this->renderPartial('_sidebar'); ?>
    </div>
    <div class="span10">
		<?php $this->widget('admin.widgets.Header'); ?>
		<div class="well">
			<div class="row-fluid">
				<dl class="span4 dl-horizontal mvn">
					<dt>Holiday Entitlement</dt>
					<dd><?php echo $allowance . ' day' . ($allowance==1?'':'s'); ?></dd>
				</dl>
				<dl class="span8 dl-horizontal mvn">
					<dt>Remaining Holiday</dt>
					<dd><?php echo $remaining . ' day' . ($remaining==1?'':'s'); ?></dd>
				</dl>
			</div>
		</div>
		 <div class="action-buttons">
			<div class="pull-left hide" id="calendar-loading"><img src="<?php echo Yii::app()->theme->baseUrl ?>/images/ajax-loader.gif" /> Loading...</div>
			<div style="text-align:right;" id="calendar-switch-buttons">
				<?php
					$year = isset($year) ? $year : date('Y');
					$this->widget('bootstrap.widgets.TbButtonGroup',array(
						'buttons'=>array(
							array(
								'icon' => 'icon-chevron-left',
								'url' => array('/hr/myholiday/index','year'=>$year-1),
							),
							array(
								'label' => $year-1,
								'url' => array('/hr/myholiday/index','year'=>$year-1),
							),
							array(
								'label' => $year,
								'url' => array('/hr/myholiday/index','year'=>$year),
								'htmlOptions' => array('class'=>'btn-primary'),
							),
							array(
								'label' => $year+1,
								'url' => array('/hr/myholiday/index','year'=>$year+1),
							),
							array(
								'icon' => 'icon-chevron-right',
								'url' => array('/hr/myholiday/index','year'=>$year+1),
							),
						),
					)); 
				?>
			</div>
		</div>
		<div class="tabbable" style="margin-top:-28px;"> <!-- Only required for left/right tabs -->
			<ul class="nav nav-tabs">
				<li class="active"><a href="#calendar-tab" data-toggle="tab">Calendar</a></li>
				<li><a href="#grid-tab" data-toggle="tab">Requests</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="calendar-tab">
					<div class="tabbable"> <!-- Only required for left/right tabs -->
			<ul class="nav nav-pills">
				<li class="active"><a href="#calendar-year-tab" data-toggle="tab"><i class="icon-calendar"></i> Year</a></li>
				<li><a href="#calendar-month-tab" data-toggle="tab"><i class="icon-calendar-empty"></i> Month</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="calendar-year-tab">
					<?php $this->widget('hr.widgets.holidaycalendar.HolidayCalendar', array('staff_id'=> $staff->id, 'year'=>$year)); ?>
				</div>
				<div class="tab-pane" id="calendar-month-tab">
					<?php $this->widget('hr.widgets.holidaycalendar.HolidayCalendar', array('staff_id'=> $staff->id, 'year'=>$year, 'view'=>'month', 'month'=>$month)); ?>
				</div>
			</div>
		</div>
				</div>
				<div class="tab-pane" id="grid-tab">
					<?php $this->renderPartial('_grid', array('year'=>$year)); ?>
				</div>
			</div>
		</div>
    </div>
</div>
<script type="text/javascript">
	jQuery(function($){
		$('#calendar-year-tab').on('click', '.mHeader', function(e){
			var selectedmonth = $(e.currentTarget).attr('data-month-select');
			$('.singleMonthView').hide();
			$('#month-number-'+selectedmonth).show();
			$('#current-visible-month').val(selectedmonth);
			$('a[href="#calendar-month-tab"]').tab('show');
		});
	});
</script>