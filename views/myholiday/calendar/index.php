<?php
// Setup Page elements
$this->title = 'Holiday Calendar';
$this->pageTitle = Yii::app()->name . ' - ' . $this->title;
$this->breadcrumbs = array(
	'Holidays' => array('/hr/holiday'),
	'Calendar',
);
$this->actionsMenu = array(
	array(
		'icon' => 'icon-chevron-left',
		'url' => array('/hr/holiday/calendar','year'=>$year-1),
	),
	array(
		'label' => $year-1,
		'url' => array('/hr/holiday/calendar','year'=>$year-1),
	),
	array(
		'label' => $year,
		'url' => array('/hr/holiday/calendar','year'=>$year),
	),
	array(
		'label' => $year+1,
		'url' => array('/hr/holiday/calendar','year'=>$year+1),
	),
	array(
		'icon' => 'icon-chevron-right',
		'url' => array('/hr/holiday/calendar','year'=>$year+1),
	),
);
?>
<div class="row-fluid">
    <div class="span2">
		<?php $this->renderPartial('_sidebar'); ?>
    </div>
    <div class="span10">
		<?php $this->widget('admin.widgets.Header'); ?>
		<?php $this->widget('hr.widgets.holidaycalendar.HolidayCalendar', array('staff_id'=> HrHelper::userStaffRecord()->id)); ?>
    </div>
</div>