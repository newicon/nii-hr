<?php
// Setup Page elements
$this->title = 'Holiday Calendar';
$this->pageTitle = Yii::app()->name . ' - ' . $this->title;
$this->breadcrumbs = array(
	'Holidays' => array('/hr/holiday'),
	'Calendar',
);
$this->actionsMenu = array(
	array(
		'icon' => 'icon-chevron-left',
		'url' => array('/hr/holiday/calendar', 'year' => $year - 1),
	),
	array(
		'label' => $year - 1,
		'url' => array('/hr/holiday/calendar', 'year' => $year - 1),
	),
	array(
		'label' => $year,
		'url' => array('/hr/holiday/calendar', 'year' => $year),
		'htmlOptions' => array('class' => 'btn-primary'),
	),
	array(
		'label' => $year + 1,
		'url' => array('/hr/holiday/calendar', 'year' => $year + 1),
	),
	array(
		'icon' => 'icon-chevron-right',
		'url' => array('/hr/holiday/calendar', 'year' => $year + 1),
	),
);
?>
<div class="row-fluid">
	<div class="span2">
		<?php $this->renderPartial('hr.views._sidebar'); ?>
	</div>
	<div class="span10">
		<?php $this->widget('admin.widgets.Header'); ?>
		
		<div class="tabbable"> <!-- Only required for left/right tabs -->
			<ul class="nav nav-pills">
				<li class="active"><a href="#calendar-year-tab" data-toggle="tab"><i class="icon-calendar"></i> Year</a></li>
				<li><a href="#calendar-month-tab" data-toggle="tab"><i class="icon-calendar-empty"></i> Month</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="calendar-year-tab">
					<?php $this->widget('hr.widgets.holidaycalendar.HolidayCalendar', array('year'=>$year)); ?>
				</div>
				<div class="tab-pane" id="calendar-month-tab">
					<?php $this->widget('hr.widgets.holidaycalendar.HolidayCalendar', array('year'=>$year, 'view'=>'month', 'month'=>$month)); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	jQuery(function($){
		$('#calendar-year-tab').on('click', '.monthWidget', function(e){
			var selectedmonth = $(e.currentTarget).attr('month');
			$('.singleMonthView').hide();
			$('#month-number-'+selectedmonth).show();
			$('#current-visible-month').val(selectedmonth);
			$('a[href="#calendar-month-tab"]').tab('show');
		});
	});
</script>