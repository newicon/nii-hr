<?php
// Setup Page elements
$this->title = 'Holidays';
$this->pageTitle = Yii::app()->name . ' - ' . $this->title;
$this->breadcrumbs = array(
	'Holidays',
);
$this->actionsMenu = array(
	array(
		'icon'=>'icon-th',
		'label'=>'Holiday Calendar',
		'url'=>array('/hr/holiday/calendar'),
	)
);
?>
<div class="row-fluid">
    <div class="span2">
		<?php $this->renderPartial('hr.views._sidebar'); ?>
    </div>
    <div class="span10">
		<?php $this->widget('admin.widgets.Header'); ?>
		<?php $this->renderPartial('_grid', array('model' => $model, 'dataProvider' => $dataProvider)); ?>
    </div>
</div>