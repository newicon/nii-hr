<?php
// Setup Page elements
$this->title = 'Edit Request';
$this->pageTitle = Yii::app()->name . ' - ' . $this->title;
$this->breadcrumbs = array(
	'Holiday' => array('/hr/holiday'),
	'Request' => array('/hr/holiday/view', 'id' => $model->id),
	'Edit',
);
$this->actionsMenu = array(
	array(
		'icon' => 'icon-trash icon-white',
		'label' => 'Delete',
		'url' => array('delete', 'id' => $model->id, 'ajax' => true),
		'type' => 'danger',
		'htmlOptions' => array('data-confirm' => 'Are you sure?', 'data-ajax' => NHtml::normalizeUrl(array('/hr/holiday'))),
	),
);
?>
<div class="row-fluid">
    <div class="span2">
		<?php $this->renderPartial('hr.views._sidebar'); ?>
    </div>
	<div class="span10">
		<?php $this->widget('admin.widgets.Header'); ?>
		<?php $this->renderPartial('_form', array('model' => $model)); ?>
	</div>
</div>