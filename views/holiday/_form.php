<?php
$disabledDates = HrHelper::holidayBookedDates();
$bankHolidays = HrHelper::bankHolidayDates();
$formAction = ($model->id) ? array('edit', 'id' => $model->id) : array('create');
$form = $this->beginWidget('NActiveForm', array(
	'id' => 'HrHolidayForm',
	'action' => $formAction,
	'enableAjaxValidation' => true,
	'enableClientValidation' => true,
	'htmlOptions' => array('class' => 'form-horizontal')
));
?>
<div class="control-group">
	<?php echo $form->labelEx($model, 'staff_id') ?>
	<div class="controls">
		<?php echo $form->dropDownList($model, 'staff_id', HrHelper::staffDropDownData(), array('empty' => '(Select a Staff Member)')); ?>
		<?php echo $form->error($model, 'staff_id'); ?>
	</div>
</div>
<a class="btn" id="add-date-row" href="javascript:void(0);"><i class="icon-plus"></i> Add Day</a>
<div id="date-rows" style="width:750px;">
	<?php $this->renderPartial('_date_row', array('count'=>1, 'date'=>null, 'disabledDates'=>$disabledDates, 'bankHolidays'=>$bankHolidays)); ?>
</div>
<div class="form-actions">
	<?php
	$cancelUrl = array('/hr/myholiday');
	echo NHtml::submitButton('Save', array('class' => 'btn btn-primary')) . '&nbsp;';
	echo NHtml::btnLink('Cancel', $cancelUrl, null, array('class' => 'btn cancel')) . '&nbsp;';
	?>		
</div>
<?php $this->endWidget(); ?>
<script type="text/javascript">
	$(function(){
		$('#add-date-row').on('click', function(e){
			var count = $('.date-row').size();
			var newcount = parseInt(count) + 1;
			var date = $('#HrHoliday_date_'+count).val();
			if (date!=null && date!='') {
				var d = new Date(date);
				d.setDate(d.getDate() + 1);
				var yyyy = d.getFullYear().toString();
				var mm = (d.getMonth()+1).toString(); // getMonth() is zero-based
				var dd  = d.getDate().toString();
				var newdate = yyyy + '-' + (mm[1]?mm:"0"+mm[0]) + '-' + (dd[1]?dd:"0"+dd[0]);
			} else {
				newdate = null;
			}
			$.get('<?php echo NHtml::url(array('/hr/myholiday/addDateRow')); ?>', {count:newcount,date:newdate}, function(response){
				$('.delete-day').hide();
				$('#date-rows').append(response);
			});
		});
		
		$('#date-rows').on('change', '.period-dropdown', function(e){
			var count = $(this).attr('data-count');
			var period = $(this).val();
			if (period=='full') {
				$('#HrHoliday_duration_'+count).val('1');
				$('#HrHoliday_half_day_period_'+count).val('');
			} else {
				$('#HrHoliday_duration_'+count).val('0.5');
				$('#HrHoliday_half_day_period_'+count).val(period);
			}
		});

		$('#date-rows').on('click', '.delete-day', function(e){
			var count = parseInt($(e.currentTarget).attr('data-count'));
			$('#delete-day-'+(count-1)).show();
			$('#date-row-'+count).remove();
		});
	});
</script>