<div class="control-group date-row" data-row-count="<?php echo $count; ?>" id="date-row-<?php echo $count; ?>">
	<div class="row-fluid">
		<div class="span6">
			<div class="control-label">Day <span id="day-count-<?php echo $count; ?>"><?php echo $count; ?></span></div>
			<div class="controls">
				<?php echo $this->widget('nii.widgets.forms.DateInput', array('name'=>'HrHoliday[date]['.$count.']', 'value'=>$date, 'disabledDates'=>$disabledDates, 'bankHolidays'=>$bankHolidays), true); ?>
			</div>
		</div>
		<div class="span5">
			<div class="control-label" style="width:80px;">Duration</div>
			<div class="controls" style="margin-left:100px;">
				<?php echo NHtml::dropDownList('period['.$count.']', null, HrHelper::durationDropDownData(), array('class'=>'period-dropdown', 'data-count'=>$count)); ?>
				<?php echo NHtml::hiddenField('HrHoliday[half_day_period]['.$count.']'); ?>
				<?php echo NHtml::hiddenField('HrHoliday[duration]['.$count.']', '1'); ?>
			</div>
		</div>
		<div class="span1">
			<?php if ($count>1)
				echo NHtml::link('<i class="icon-remove"></i>', 'javascript:void(0);', array('class'=>'delete-day', 'data-count'=>$count, 'id'=>'delete-day-'.$count, 'style'=>'line-height:30px;margin-left:16px;'));
			?>
		</div>
	</div>
</div>