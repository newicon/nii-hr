<div class="well" style="margin-top:15px;padding:8px 0">
	<?php
	$this->widget('bootstrap.widgets.TbMenu', array(
		'type' => 'list',
		'items' => array(
			array('label' => 'Human Resources'),
			array('label' => 'Staff', 'url' => array('/hr/staff')),
			array('label' => 'Holiday Requests', 'url' => array('/hr/holiday/index')),
			array('label' => 'Holiday Calendar', 'url' => array('/hr/holiday/calendar')),
		),
	));
	?>
</div>