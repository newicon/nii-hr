<?php
// Setup Page elements
$this->title = 'Staff';
$this->pageTitle = Yii::app()->name . ' - ' . $this->title;
$this->breadcrumbs = array(
	'Staff',
);
$this->actionsMenu = array(
	array(
		'icon' => 'icon-plus',
		'label' => 'Create a Staff Member',
		'url' => array('create'),
	),
);
?>
<div class="row-fluid">
    <div class="span2">
		<?php $this->renderPartial('hr.views._sidebar'); ?>
    </div>
    <div class="span10">
		<?php $this->widget('admin.widgets.Header'); ?>
		<?php $this->renderPartial('_grid', array('model' => $model, 'dataProvider' => $dataProvider)); ?>
    </div>
</div>