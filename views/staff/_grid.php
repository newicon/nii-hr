<?php
$this->widget('nii.widgets.grid.NGridView', array(
	'dataProvider' => $dataProvider,
	'filter' => $model,
	'id' => 'HrStaffGrid',
	'columns' => array(
		array(
			'name'=>'name',
			'type'=>'raw',
			'value'=>'HrHelper::staffViewLink($data)'
		),
		'job_title',
		'department'
	)
));
?>
