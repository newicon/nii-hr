<?php
// Setup Page elements
$this->title = $model->name;
$this->pageTitle = Yii::app()->name . ' - ' . $this->title;
$this->breadcrumbs = array(
	'Staff' => array('/hr/staff'),
	$model->name,
);
$this->actionsMenu = array(
	array(
		'icon' => 'icon-pencil',
		'label' => 'Edit',
		'url' => array('edit', 'id' => $model->id),
	),
);

$years = array(date('Y'), date('Y', strtotime('+1 year')), date('Y', strtotime('+2 year')));
foreach ($years as $year) {
	$allowance[$year] = HrHelper::getStaffHolidayAllowance($year, $model) * 1;
	$remaining[$year] = $model->getRemainingHoliday($year);
}
?>
<div class="row-fluid">
    <div class="span2">
		<?php $this->renderPartial('hr.views._sidebar'); ?>
    </div>
	<div class="span10">
		<?php $this->widget('admin.widgets.Header'); ?>
		<div class="well">
			<div class="row-fluid">
				<div class="span6">
					<dl class="dl-horizontal">
						<dt>Job Title</dt>
						<dd><?php echo $model->job_title; ?></dd>
						<dt>Manager</dt>
						<dd><?php echo $model->manager ? HrHelper::staffViewLink($model->manager) : '&nbsp'; ?></dd>
						<dt style="margin-top:10px;">Year</dt>
						<dd>
							<div class="row-fluid" style="font-weight: bold;margin-top:10px;text-align:center;">
								<?php foreach ($years as $year) { ?>
									<div class="span3"><?php echo $year; ?></div>
								<?php } ?>
							</div>
						</dd>
						<dt>Holiday Entitlement<br>(exc Christmas)<br>&nbsp;</dt>
						<dd>
							<div class="row-fluid" style="text-align:center;">
								<?php foreach ($years as $year) { ?>
									<div class="span3"><?php echo $allowance[$year] . ' day' . ($allowance[$year] == 1 ? '' : 's'); ?></div>
								<?php } ?>
							</div>
						</dd>
						<dt>Remaining Holiday</dt>
						<dd>
							<div class="row-fluid" style="text-align:center;">
								<?php foreach ($years as $year) { ?>
									<div class="span3"><?php echo $remaining[$year] . ' day' . ($remaining[$year] == 1 ? '' : 's'); ?></div>
								<?php } ?>
							</div>
						</dd>
					</dl>
				</div>
			</div>
		</div>
		<div class="tabbable"> <!-- Only required for left/right tabs -->
			<ul class="nav nav-tabs">
				<li class="active"><a href="#notes-tab" data-toggle="tab">Notes</a></li>
				<li><a href="#holidays-tab" data-toggle="tab">Holidays</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="notes-tab">
					<?php $this->renderPartial('notes/index', array('model' => $model)); ?>
				</div>
				<div class="tab-pane" id="holidays-tab">
					<?php $this->renderPartial('holiday/_grid', array('model' => $model)); ?>
				</div>
			</div>
		</div>
	</div>
</div>