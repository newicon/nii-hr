<?php
// Setup Page elements
$this->title = 'Create a Holiday';
$this->pageTitle = Yii::app()->name . ' - ' . $this->title;
$this->breadcrumbs = array(
	'Staff' => array('/hr/staff'),
	$staff->name => array('/hr/staff/view', 'id' => $staff->id),
	'Create a Holiday',
);
?>
<div class="row-fluid">
    <div class="span2">
		<?php $this->renderPartial('hr.views._sidebar'); ?>
    </div>
	<div class="span10">
		<?php $this->widget('admin.widgets.Header'); ?>
		<?php $this->renderPartial('holiday/_form', array('model' => $model, 'staff' => $staff)); ?>
	</div>
</div>