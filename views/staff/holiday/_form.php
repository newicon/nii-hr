<?php
$formAction = ($model->id) ? array('editholiday', 'staff' => $staff->id, 'id' => $model->id) : array('createholiday', 'staff' => $staff->id);
$form = $this->beginWidget('NActiveForm', array(
	'id' => 'HrHolidayForm',
	'action' => $formAction,
	'enableAjaxValidation' => true,
	'enableClientValidation' => true,
	'htmlOptions' => array('class' => 'form-horizontal')
));
?>
<div class="control-group">
	<?php echo $form->labelEx($model, 'date') ?>
	<div class="controls">
		<?php echo $form->dateField($model, 'date'); ?>
		<?php echo $form->error($model, 'date'); ?>
	</div>
</div>
<div class="control-group">
	<?php echo $form->labelEx($model, 'duration') ?>
	<div class="controls">
		<?php echo $form->dropDownList($model, 'duration', HrHelper::durationDropDownData()); ?>
		<?php echo $form->error($model, 'duration'); ?>
	</div>
</div>
<div class="form-actions">
	<?php
	$cancelUrl = array('/hr/holiday');
	echo NHtml::submitButton('Save', array('class' => 'btn btn-primary')) . '&nbsp;';
	echo NHtml::btnLink('Cancel', $cancelUrl, null, array('class' => 'btn cancel')) . '&nbsp;';
	?>		
</div>
<?php $this->endWidget(); ?>