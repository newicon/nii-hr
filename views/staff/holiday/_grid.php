<?php
$gridmodelName = 'HrStaffHoliday';
$gridmodel = new $gridmodelName('search');
$gridmodel->unsetAttributes();

if (isset($_GET[$gridmodelName]))
	$gridmodel->attributes = $_GET[$gridmodelName];

$gridmodel->staff_id = $model->id;

$dataProvider = $gridmodel->search();
$dataProvider->pagination->pageSize = 5;

$click = <<<EOD
function() {
	var th = this,
		after = function(){};
	jQuery('#HrHolidayGrid').yiiGridView('update', {
		type: 'POST',
		url: jQuery(this).attr('href'),
		success: function(data) {
			jQuery('#HrHolidayGrid').yiiGridView('update');
			after(th, true, data);
		},
		error: function(XHR) {
			return after(th, false, XHR);
		}
	});
	return false;
}
EOD;

$this->widget('nii.widgets.grid.NGridView', array(
	'dataProvider' => $dataProvider,
	'filter' => $gridmodel,
	'id' => 'HrHolidayGrid',
	'columns' => array(
		array(
			'name'=>'dates',
			'type'=>'raw',
			'filter'=>false,
			'value'=>'HrHelper::holidayRequestManagerLink($data)',
		),
		array(
			'name'=>'duration',
			'type'=>'raw',
			'filter'=>false,
			'value'=>'$data->requestDuration',
			'htmlOptions'=>array('width'=>70),
		),
		array(
			'name' => 'state',
			'value' => '$data->stateLabel',
			'filter'=> HrHelper::requestStateDropdown(),
		),
		array(
			'name' => 'requested_at',
			'type' => 'raw',
			'value' => 'NHtml::formatDate($data->requested_at)',
		),
		array(
			'class' => 'NButtonColumn',
			'headerHtmlOptions' => array('width' => 60),
			'htmlOptions' => array('width' => 60),
			'deleteButtonOptions' => array('class' => 'icon-trash', 'title' => 'Trash'),
			'deleteButtonLabel' => '',
			'deleteButtonImageUrl' => false,
			'deleteButtonUrl' => 'Yii::app()->controller->createUrl("/hr/holiday/delete",array("id"=>$data->primaryKey))',
			'template' => '{delete}',
			'buttons' => array(
				'delete' => array(
					'visible' => 'HrHelper::isManager($data->staff)',
				),
			),
		),
	),
	'scopes' => array(
		'scopeUrl' => NHtml::normalizeUrl(array('/hr/staff/view','id'=>$model->id)),
		'default' => 'requested',
		'items' => array(
			'requested' => 'Requested',
			'approved' => 'Approved',
			'rejected' => 'Rejected',
			'default' => 'All',
		),
	),
));
?>