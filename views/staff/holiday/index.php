<?php $this->renderPartial('holiday/_grid', array('model' => $model)); ?>
<div class="action-buttons">
	<?php
	$this->widget('bootstrap.widgets.TbButtonGroup', array(
		'buttons' => array(
			array(
				'icon' => 'icon-plus',
				'label' => 'Add a Holiday',
				'url' => array('/hr/staff/createholiday', 'staff' => $model->id),
			)
		),
	));
	?>
</div>