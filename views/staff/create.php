<?php
// Setup Page elements
$this->title = 'Create a Staff Member';
$this->pageTitle = Yii::app()->name . ' - ' . $this->title;
$this->breadcrumbs = array(
	'Staff' => array('/hr/staff'),
	'Create',
);
?>
<div class="row-fluid">
    <div class="span2">
		<?php $this->renderPartial('hr.views._sidebar'); ?>
    </div>
	<div class="span10">
		<?php $this->widget('admin.widgets.Header'); ?>
		<?php $this->renderPartial('_form', array('model' => $model)); ?>
	</div>
</div>