<?php
$formAction = ($model->id) ? array('edit', 'id' => $model->id) : array('create');
$form = $this->beginWidget('NActiveForm', array(
	'id' => 'HrStaffForm',
	'action' => $formAction,
	'enableAjaxValidation' => true,
	'enableClientValidation' => true,
	'htmlOptions' => array('class' => 'form-horizontal')
));
$years = array(date('Y'), date('Y', strtotime('+1 year')), date('Y', strtotime('+2 year')));
foreach ($years as $year)
	$allowance[$year] = HrHelper::getStaffHolidayAllowance($year, $model);
?>
<div class="control-group">
	<?php echo $form->labelEx($model, 'user_id') ?>
	<div class="controls">
		<?php echo $form->dropDownList($model, 'user_id', HrHelper::userDropDownData(), array('empty' => '(Select a User)')); ?>
		<?php echo $form->error($model, 'user_id'); ?>
	</div>
</div>
<div class="control-group">
	<?php echo $form->labelEx($model, 'manager_id') ?>
	<div class="controls">
		<?php echo $form->dropDownList($model, 'manager_id', HrHelper::managerDropDownData($model->id), array('empty' => '(Select a Manager)')); ?>
		<?php echo $form->error($model, 'manager_id'); ?>
	</div>
</div>
<div class="control-group">
	<?php echo $form->labelEx($model, 'job_title') ?>
	<div class="controls">
		<?php echo $form->textField($model, 'job_title'); ?>
		<?php echo $form->error($model, 'job_title'); ?>
	</div>
</div>
<div class="control-group">
	<?php echo $form->labelEx($model, 'department') ?>
	<div class="controls">
		<input type="text" name="HrStaff[department]" list="exampleList">
		<datalist id="exampleList">
			<?php foreach(HrStaff::getDepartments() as $department): ?>
				<option value="<?php echo $department ?>">
			<?php endforeach ?>
		</datalist>
		<?php echo $form->error($model, 'department'); ?>
	</div>
</div>
<div class="control-group">
	<?php echo $form->labelEx($model, 'employed') ?>
	<div class="controls">
		<?php echo $form->checkbox($model, 'employed') ?>
		<?php echo $form->error($model, 'employed'); ?>
	</div>
</div>
<div class="well w600">
	<h4 class="mbl">Holiday Allowance (exc Christmas)</h4>
	<div class="control-group">
		<?php echo $form->labelEx($model, 'holiday_allowance', array('label'=>"Default Allowance",'class' => 'control-label')) ?>
		<div class="controls">
			<div class="input-append">
				<?php echo $form->textField($model, 'holiday_allowance', array('class'=>'input-mini')); ?>
				<div class="add-on">days</div>
			</div>

			<?php echo $form->error($model, 'holiday_allowance'); ?>
		</div>
	</div>
	<?php foreach ($years as $year) { ?>
		<div class="control-group">
			<div class="control-label"><strong><?php echo $year; ?></strong> Allowance</div>
			<div class="controls">
				<div class="input-append">
					<?php echo NHtml::textField('holiday_allowance['.$year.']', $allowance[$year], array('class'=>'input-mini')); ?>
					<div class="add-on">days</div>
				</div>
			</div>
		</div>
	<?php } ?>
</div>
<div class="form-actions">
	<?php
	$cancelUrl = ($model->id) ? array('staff/view', 'id' => $model->id) : array('/hr/staff');
	echo NHtml::submitButton('Save', array('class' => 'btn btn-primary')) . '&nbsp;';
	echo NHtml::btnLink('Cancel', $cancelUrl, null, array('class' => 'btn cancel')) . '&nbsp;';
	?>
</div>
<?php $this->endWidget(); ?>
